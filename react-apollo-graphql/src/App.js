import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { AllWidgetsQuery, ALL_WIDGETS_QUERY } from './queries/AllWidgetsQuery';
import { DeleteWidgetMutation } from './mutations/DeleteWidgetMutation';

import { WidgetList } from './components/WidgetList';
import { WidgetForm } from './components/WidgetForm';

const APP_QUERY = gql`
  query AppQuery {
    widgets {
      id
      name
    }
  }
`;

const APPEND_WIDGET_MUTATION = gql`
  mutation AppendWidget($widget: WidgetInput) {
    appendWidget(widget: $widget) {
      id
      name
    }
  }
`;


// fetch('http://localhost:3030', {
//   method: 'POST',
//   headers: { 'Content-Type': 'application/json' },
//   body: JSON.stringify({
//     query: 'query AppQuery { widgets { id name } }',
//     variables: null,
//     operationName: 'AppQuery'
//   })
// }).then(res => res.json()).then(widgets => console.log(widgets));

export const App = () => {

  return <>
    <AllWidgetsQuery
      component={(props) => <DeleteWidgetMutation
        {...props}
        refetchQueries={[ { query: ALL_WIDGETS_QUERY } ]}
        component={props => <WidgetList {...props} />} />}
    /> 
    <Mutation mutation={APPEND_WIDGET_MUTATION}>
      {(mutate) => {

        const appendWidget = widget => {
          return mutate({
            mutation: APPEND_WIDGET_MUTATION,
            variables: { widget },
            // refetchQueries: [
            //   { query: APP_QUERY,}
            // ],
            optimisticResponse: {
              appendWidget: {
                ...widget,
                id: -1,
                __typename: 'Widget',
              }
            },
            update(store, { data: { appendWidget: widget }}) {
              let data = store.readQuery({ query: APP_QUERY });
              data = { ...data, widgets: data.widgets.concat(widget) };
              store.writeQuery({ query: APP_QUERY, data });
            }
          });
        };

        return <WidgetForm onSubmitWidget={appendWidget} />;
        
      }}

    </Mutation>
  </>;

};