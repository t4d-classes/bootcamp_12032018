import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

const DELETE_WIDGET_MUTATION = gql`
  mutation DeleteWidgetMutation($widgetId: Int) {
    deleteWidget(widgetId: $widgetId)
  }
`;

export const DeleteWidgetMutation = (props) => {

  return <Mutation mutation={DELETE_WIDGET_MUTATION}>
    {(mutate) => {

      const deleteWidget = widgetId => {
        mutate({
          variables: { widgetId },
          refetchQueries: props.refetchQueries,
        });
      };

      const PresentationalComponent = props.component;

      return <PresentationalComponent onDeleteWidget={deleteWidget} {...props} />;

    }}
  </Mutation>;

};