import React from 'react';
import PropTypes from 'prop-types';

export const WidgetList = ({ widgets, onDeleteWidget }) => {

  return <ul>
    {widgets.map(w => <li key={w.id}>
      {w.id} {w.name}
      <button type="button" onClick={() => onDeleteWidget(w.id)}>Delete</button>
    </li>)}
  </ul>;

};

WidgetList.defaultProps = {
  widgets: [],
};

WidgetList.propTypes = {
  widgets: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    color: PropTypes.string,
    size: PropTypes.string,
    price: PropTypes.number,
    quantity: PropTypes.number,
  })),
  onDeleteWidget: PropTypes.func.isRequired,
};