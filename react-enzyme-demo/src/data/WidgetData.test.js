import WidgetData from './WidgetData';

jest.mock('./WidgetData');

describe('WidgetData Mocking Demo', () => {

  test('Get All Widgets', () => {

    const widgetData = new WidgetData();

    return widgetData.all().then(widgets => expect(widgets.length).toEqual(2));
  });

});