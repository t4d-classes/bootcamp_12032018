export class WidgetData {

  constructor(restURL) {
    this._restURL = restURL;
  }

  all() {
    return fetch(`${this._restURL}/widgets`).then(res => res.json());
  }

  one(widgetId) {
    return fetch(`${this._restURL}/widgets/${encodeURIComponent(widgetId)}`).then(res => res.json());
  }

  append(widget) {
    return fetch(`${this._restURL}/widgets`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(widget),
    }).then(res => res.json());
  }

  replace(widget) {
    return this.one(widget.id).then(oldWidget =>
      fetch(`${this._restURL}/widgets/${encodeURIComponent(widget.id)}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(widget),
        }).then(() => oldWidget));
  }

  delete(widgetId) {
    return this.one(widgetId).then(widget =>
      fetch(`${this._restURL}/widgets/${encodeURIComponent(widgetId)}`, {
        method: 'DELETE',
      }).then(() => widget));
  }

}

export default WidgetData;