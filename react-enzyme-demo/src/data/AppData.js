import WidgetData from './WidgetData';

export class AppData {

  allWidgets() {
    return new WidgetData().all();
  }

}

export default AppData; 