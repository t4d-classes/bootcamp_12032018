export const mockAll = jest.fn(() => {

  return Promise.resolve([
    { id: '1', name: 'Test Widget 1', description: 'Test Widget Desc',
        color: 'red', size: 'small', price: 10.0, quantity: 5, __typename: 'Widget' },
    { id: '2', name: 'Test Widget 2', description: 'Test Widget Desc',
      color: 'red', size: 'small', price: 10.0, quantity: 5, __typename: 'Widget' },
  ]);

});

const mock = jest.fn().mockImplementation(() => {
  return { all: mockAll };
});

export default mock;