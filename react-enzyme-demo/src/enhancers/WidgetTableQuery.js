import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetTable } from '../components/WidgetTable';

export const WIDGET_TABLE_QUERY = gql`
  query WidgetTableQuery {
    app @client {
      id
      toolName
      editWidgetId
    }
    widgets {
      ...widgetRow
    }
  }
  
  ${WidgetTable.fragments.widgetTable}
`;

export const withWidgetTableQuery = graphql(WIDGET_TABLE_QUERY, {
  props: ({ data }) => {
    
    if (data.loading) {
      return {
        toolName: '',
        editWidgetId: '-1',
        widgets: [],
        loading: data.loading,
        error: data.error,
        widgetRefreshQuery: WIDGET_TABLE_QUERY,
      };
    }

    return {
      toolName: data.app.toolName,
      editWidgetId: data.app.editWidgetId,
      widgets: data.widgets,
      loading: data.loading,
      error: data.error,
      widgetRefreshQuery: WIDGET_TABLE_QUERY,
    };
  },
});
