import * as React from "react";
import { mount } from 'enzyme';
import { ApolloConsumer } from 'react-apollo';
import { MockedProvider } from "react-apollo/test-utils";
import wait from "waait";

import { withDeleteWidgetMutation, DELETE_WIDGET_MUTATION } from "./DeleteWidgetMutation";
import { WIDGET_TABLE_QUERY } from './WidgetTableQuery';

const DeleteWidgetMutationContainer = withDeleteWidgetMutation(props => {
  return <div>
    <button type="button" onClick={() => props.onDeleteWidget('1')}>Delete</button>
  </div>;
});

describe('DeleteWidgetMutation Tests', () => {

  test("should render without error", async (done) => {

    const deleteWidgetMutationMock = {
      request: { query: DELETE_WIDGET_MUTATION, variables: { widgetId: "1" } },
      result: { data: { deleteWidget: {
        id: '1', name: 'Test Widget', description: 'Test Widget Desc',
        color: 'red', size: 'small', price: 10.0, quantity: 5, __typename: 'Widget',
      } } }
    };

    const widgetTableQueryMock = {
      request: { query: WIDGET_TABLE_QUERY },
      result: { data: {
        app: { id: '1', editWidgetId: '-1', toolName: 'Test Tool', __typename: 'App' },
        widgets: [
          { id: '1', name: 'Test Widget 1', description: 'Test Widget Desc',
            color: 'red', size: 'small', price: 10.0, quantity: 5, __typename: 'Widget' },
          { id: '2', name: 'Test Widget 2', description: 'Test Widget Desc',
            color: 'red', size: 'small', price: 10.0, quantity: 5, __typename: 'Widget' },
        ] 
      } },
    }

    let theClient = null;

    const component = mount(
      <MockedProvider mocks={[ deleteWidgetMutationMock ]} addTypename={true}>
        <ApolloConsumer>
          {client => {

            theClient = client;
            
            client.writeQuery({
              query: WIDGET_TABLE_QUERY, 
              data: widgetTableQueryMock.result.data
            });
            
            return <DeleteWidgetMutationContainer widgetRefreshQuery={WIDGET_TABLE_QUERY}
              onCancelWidget={() => done() } />
          }}
        </ApolloConsumer>
      </MockedProvider>
    );
    
    const data1 = theClient.readQuery({ query: WIDGET_TABLE_QUERY });
    expect(data1.widgets.length).toBe(2);

    component.find('button').simulate('click');

    await wait(0);

    const data2 = theClient.readQuery({ query: WIDGET_TABLE_QUERY });
    expect(data2.widgets.length).toBe(1);
  });
  
});
