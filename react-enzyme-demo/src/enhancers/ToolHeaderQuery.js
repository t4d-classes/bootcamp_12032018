import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

export const TOOL_HEADER_QUERY = gql`query ToolHeaderQuery { app @client { id toolName } }`;

export const withToolHeaderQuery = graphql(
  TOOL_HEADER_QUERY,
  { props: ({ data }) => {

    if (data.loading) {
      return { headerText: '' };
    }

    return ({ headerText: data.app.toolName });
  } },
);