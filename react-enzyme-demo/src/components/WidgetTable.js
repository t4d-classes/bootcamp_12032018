import * as React from 'react';

import { WidgetViewRow } from './WidgetViewRow';
import { WidgetEditRow } from './WidgetEditRow';

export const WidgetTable = ({
  widgets, editWidgetId, onDeleteWidget,
  onEditWidget, onSaveWidget, onCancelWidget,
}) =>
  <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Color</th>
        <th>Size</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
      {widgets.map(widget => widget.id === editWidgetId
          ? <WidgetEditRow key={widget.id} widget={widget} onSaveWidget={onSaveWidget} onCancelWidget={onCancelWidget} />
          : <WidgetViewRow key={widget.id} widget={widget} onEditWidget={onEditWidget} onDeleteWidget={onDeleteWidget} />
      )}
    </tbody>
  </table>;

WidgetTable.fragments = {
  widgetTable: `
    ${WidgetViewRow.fragments.widgetRow}
  `,
  };
