import * as React from 'react';
import { render, mount, shallow } from 'enzyme';

import { WidgetForm } from './WidgetForm';

describe('<WidgetForm /> Enzyme Static HTML', () => {

  test('<WidgetForm /> renders', () => {
    
    const component = JSON.stringify(render(
      <WidgetForm onSubmitWidget={() => null} buttonText="Add Widget" />
    ).html());
    
    expect(component).toMatchSnapshot();
  });

});

describe('<WidgetForm /> Enzyme Mock DOM', () => {

  const eventHandlers = {
    submitWidget: () => null,
  };

  let submitWidgetSpy;
  let component;

  beforeEach(() => {

    submitWidgetSpy = jest.spyOn(eventHandlers, 'submitWidget');

    component = mount(
      <WidgetForm buttonText="Add Widget"
        onSubmitWidget={eventHandlers.submitWidget} />
    );

  });

  test('<WidgetForm /> renders', () => {

    expect(component.find('button').text()).toBe('Add Widget');

  });

  test('<WidgetForm /> form and submit widget button', () => {

    const widget = {
      name: 'Test Widget',
      description: 'Test Widget Desc',
      color: 'red',
      size: 'small',
      price: 10.00,
      quantity: 15
    };

    Object.keys(widget).forEach(propName => {
      component.find(`#${propName}-input`).simulate('change',
        { target: { value: widget[propName], name: propName } });
    });

    component.find('button').simulate('click');

    expect(submitWidgetSpy).toHaveBeenCalledWith(widget);

  });

});

describe('<WidgetViewRow /> Shallow with Enzyme', () => {

  const eventHandlers = {
    submitWidget: () => null,
  };

  let submitWidgetSpy;
  let wrapper;

  beforeEach(() => {

    submitWidgetSpy = jest.spyOn(eventHandlers, 'submitWidget');

    wrapper = shallow(<WidgetForm buttonText="Add Widget"
      onSubmitWidget={eventHandlers.submitWidget} />);

  });

  test('<WidgetForm /> renders', () => {

    expect(wrapper.find('button').text()).toBe('Add Widget');

  });

  test('<WidgetForm /> form and submit widget button', () => {

    const widget = {
      name: 'Test Widget',
      description: 'Test Widget Desc',
      color: 'red',
      size: 'small',
      price: 10.00,
      quantity: 15
    };

    Object.keys(widget).forEach(propName => {
      wrapper.find(`#${propName}-input`).simulate('change',
        { target: { value: widget[propName], name: propName } });
    });

    wrapper.find('button').simulate('click');

    expect(submitWidgetSpy).toHaveBeenCalledWith(widget);

  });


});