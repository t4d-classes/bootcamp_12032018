import * as React from 'react';
import { render, mount, shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';

import { ToolHeader } from './ToolHeader';

describe('<ToolHeader /> Enzyme Static HTML', () => {

  test('<ToolHeader /> renders', () => {
    
    const component = JSON.stringify(render(
      <MemoryRouter>
        <ToolHeader headerText="The Tool" />
      </MemoryRouter>
    ).html());
    
    expect(component).toMatchSnapshot();
  });

});

describe('<ToolHeader /> Enzyme Mock DOM', () => {

  let component;
  let componentDOMNode;

  beforeEach(() => {

    component = mount(<MemoryRouter>
      <ToolHeader headerText="The Tool" />
    </MemoryRouter>);
    
    componentDOMNode = component.find('h1');
  
  });

  test('<ToolHeader /> renders', () => {
    expect(componentDOMNode.text()).toBe('The Tool');
  });

});

describe('<ToolHeader /> Shallow with Enzyme', () => {

  let wrapper;

  beforeEach(() => {

    wrapper = shallow(<ToolHeader headerText="The Tool" />);

  });

  test('<ToolHeader /> renders', () => {

    const h1Content = wrapper.props().children[0].props.children;

    expect(h1Content).toBe('The Tool');
  
  });

});
