import * as React from 'react';
import { Link } from 'react-router-dom';

export const ToolHeader = ({ headerText }) =>
  <header>
    <h1>{headerText}</h1>
    <nav>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/create">Create</Link></li>
      </ul>
    </nav>
  </header>;