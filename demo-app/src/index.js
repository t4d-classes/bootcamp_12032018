import React from 'react';
import ReactDOM from 'react-dom';

let counter = 0;
class ListItem extends React.Component {
  
  constructor(props) {
    super(props);

    this.counter = counter++;

    console.log('list item constructor: ', this.counter);

    this.state = {
      item: props.item
    };
  }

  componentWillUnmount() {
    console.log('list item will unmount: ', this.counter);
  }

  render() {
    return <li>props: {this.props.item}, state: {this.state.item}</li>;
  }

}
class ItemList extends React.Component {

  constructor(props) {
    super(props);
    console.log('item list constructor');
  }

  render() {
    return <ul>
      {this.props.items.map( (item) => <ListItem key={item} item={item} />)}
    </ul>
  }
}

ReactDOM.render(<ItemList items={[ 'red', 'green', 'blue' ]} />, document.querySelector('#root'));

setTimeout(() => {
  ReactDOM.render(<ItemList items={[ 'red', 'blue' ]} />, document.querySelector('#root'));
}, 3000);