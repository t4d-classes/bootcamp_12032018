import { EDIT_CAR, CANCEL_CAR } from '../actions/carToolActions';
import { REFRESH_CARS_DONE } from '../actions/refreshCars';

export const loadingReducer = (state = false, action) => {
  if (action.type.endsWith('REQUEST')) {
    return true;
  }
  if (action.type.endsWith('DONE')) {
    return false;
  }
  return state;
};

export const editCarIdReducer = (state = -1, action) => {
  if (action.type === EDIT_CAR) {
    return action.payload;
  }
  if (action.type === CANCEL_CAR || action.type.endsWith('REQUEST')) {
    return -1;
  }
  return state;
};

export const carsReducer = (state = [], action) => {
  if (action.type === REFRESH_CARS_DONE) {
    return action.payload;
  }
  return state;
}

// export const carToolReducer = (state = {
//   loading: false,
//   cars: [],
//   editCarId: -1
// }, action) => {

//   switch (action.type) {
//     case REFRESH_CARS_REQUEST:
//     case DELETE_CAR_REQUEST:
//       return { ...state, loading: true };
//     case REFRESH_CARS_DONE: 
//       return { ...state, loading: false, cars: action.payload };
//     case EDIT_CAR:
//       return { ...state, editCarId: action.payload };
//     case CANCEL_CAR:
//       return { ...state, editCarId: -1 };
//     default:
//       return state;
//   }


// };