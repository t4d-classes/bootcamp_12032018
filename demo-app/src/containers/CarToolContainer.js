import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { CarTool } from '../components/CarTool';

import {
  createAddCarAction,
  createEditCarAction,
  createReplaceCarAction,
  createCancelCarAction
} from '../actions/carToolActions';
import { refreshCars } from '../actions/refreshCars';
import { deleteCar } from '../actions/deleteCar';

export const CarToolContainer = connect(
  ({ cars, editCarId }) => ({ cars, editCarId }),
  dispatch => bindActionCreators({
    refreshCars,
    addCar: createAddCarAction,
    replaceCar: createReplaceCarAction,
    deleteCar,
    editCar: createEditCarAction,
    cancelCar: createCancelCarAction,
  }, dispatch),
)(CarTool);