import React from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export class CarTool extends React.Component {

  componentDidMount() {
    this.props.refreshCars();
  }

  render() {

    return <>
      <ToolHeader headerText="Car Tool" />
      <CarTable cars={this.props.cars} editCarId={this.props.editCarId}
        onEditCar={this.props.editCar} onDeleteCar={this.props.deleteCar}
        onSaveCar={this.props.replaceCar} onCancelCar={this.props.cancelCar} />
      <CarForm onSubmitCar={this.props.addCar} buttonText="Add Car" />
    </>;
  }


}
