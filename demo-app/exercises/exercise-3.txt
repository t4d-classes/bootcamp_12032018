Exercise 3

1. Create an array of car objects using the car data which you created in the previous exercise.

2. Using the array of car objects, dynamically build the table rows in table of cars.

3. Ensure it works with no errors.